# mountS3-EC2



Для монтирования файловой системы S3 bucket в запускающийся EC2 instance можно использовать инструмент `s3fs`, который позволяет монтировать S3 bucket как файловую систему в Linux. 

Ниже приведен пример скрипта, который можно использовать для этой задачи. Этот скрипт можно добавить в `user-data` при создании EC2 instance, чтобы он выполнялся автоматически при запуске.

```bash
#!/bin/bash
# Update the package list and install dependencies
sudo yum update -y
sudo yum install -y automake fuse fuse-devel gcc-c++ git libcurl-devel libxml2-devel make openssl-devel

# Install s3fs
git clone https://github.com/s3fs-fuse/s3fs-fuse.git
cd s3fs-fuse
./autogen.sh
./configure --prefix=/usr --with-openssl
make
sudo make install

# Set up S3 credentials (ensure you replace <your-access-key-id> and <your-secret-access-key> with your actual credentials)
echo "<your-access-key-id>:<your-secret-access-key>" > ${HOME}/.passwd-s3fs
chmod 600 ${HOME}/.passwd-s3fs

# Create a directory for the S3 bucket mount
sudo mkdir /mnt/s3bucket

# Mount the S3 bucket (replace <your-bucket-name> with the actual bucket name)
sudo s3fs <your-bucket-name> /mnt/s3bucket -o passwd_file=${HOME}/.passwd-s3fs -o url=https://s3.amazonaws.com -o use_path_request_style

# Add the mount to /etc/fstab to remount on reboot
echo "s3fs#<your-bucket-name> /mnt/s3bucket fuse _netdev,allow_other,passwd_file=${HOME}/.passwd-s3fs,use_path_request_style,url=https://s3.amazonaws.com 0 0" | sudo tee -a /etc/fstab
```

### Объяснение скрипта:
1. **Обновление и установка зависимостей:** Сначала обновляются пакеты и устанавливаются необходимые зависимости.
2. **Установка `s3fs`:** Затем клонируется репозиторий `s3fs-fuse`, и `s3fs` собирается и устанавливается.
3. **Настройка учетных данных S3:** Создается файл с учетными данными для доступа к S3, и устанавливаются правильные разрешения.
4. **Создание каталога для монтирования:** Создается каталог `/mnt/s3bucket`, куда будет монтироваться S3 bucket.
5. **Монтирование S3 bucket:** S3 bucket монтируется в созданный каталог.
6. **Добавление в `/etc/fstab`:** Запись добавляется в `/etc/fstab` для автоматического монтирования при перезагрузке системы.

Этот скрипт следует использовать с осторожностью, и лучше всего проверять его на тестовых инстансах перед использованием в продакшн-среде. Также следует использовать IAM роли для EC2 инстансов для повышения безопасности, вместо хранения учетных данных в файле.
